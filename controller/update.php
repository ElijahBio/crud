<?php
$timestamp = date( 'Y-m-d H:i:s', time());
$pdo = connect();
if (isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    $stmt = $pdo->prepare('UPDATE article SET name = :name, description = :description, created_at = :created_at WHERE id = :id');
    $stmt->bindValue(':id', $_GET['id']);
    $stmt->bindValue(':name', $_POST['name']);
    $stmt->bindValue(':description', $_POST['description']);
    $stmt->bindValue(':created_at', $_POST['created_at']);

    header('Location: index.php');
}
if (isset($_GET['id'])) {
    $stmt = $pdo->prepare('SELECT * FROM article WHERE id = :id');
    $stmt->bindValue(':id', $_GET['id']);
    $stmt->execute();
    $article = $stmt->fetch();
} else {
    header('Location: index.php');
}

require_once __DIR__."/../view/update.php";