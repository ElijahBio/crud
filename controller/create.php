<?php

$timestamp = date( 'Y-m-d H:i:s', time());
if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['created_at'])) {
    // передаем данные в модель
    insert($_POST['name'], $_POST['description'], $_POST['created_at']);
    header('Location: index.php');
}

$article['created_at'] = "";
$article['description'] = "";
$article['name'] = "";
//отдаем юзеру view
require_once __DIR__."/../view/update.php";