<?php

function connect(){
    $pdo = new PDO("pgsql:host=127.0.0.1;dbname=postgres", 'postgres', 'Alex2134');

    return $pdo;
}

function findAll(){
    $pdo = connect();
    $stmt = $pdo->prepare('SELECT * FROM article');
    $stmt->execute();
    $articles = $stmt->fetchAll();

    return $articles;
}

function insert($name, $description, $created_at){
    $pdo = connect();
    $stmt = $pdo->prepare('INSERT INTO article (name, description, created_at) VALUES (:name,:description,:created_at)');
    $stmt->bindValue(':name', $name);
    $stmt->bindValue(':description', $description);
    $stmt->bindValue(':created_at', $created_at);
    $stmt->execute();


}